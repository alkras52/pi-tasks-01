#include <stdio.h>

int main()
{
    char line[256];
    int i = 0, sum = 0, num = 0;
    printf("Enter a line, please \n");
    fgets(line, 256, stdin);
    
    while (line[i])
    {
        if ( (line[i] <= '9') && (line[i] >= '0') && (num == 0) )
            num = line[i] - '0';
        if ( (line[i+1] <= '9') && (line[i+1] >= '0') )
        {
            num *= 10;
            num += (line[i+1] - '0');
        }
        else
        {
            sum += num;
            num = 0;
        }
        ++i;
    }
    printf("Sum of line = %d \n", sum);
    return 0;
}
