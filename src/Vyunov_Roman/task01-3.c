#define STRING_LENGTH 256

#include <stdio.h>

int main()
{
	int sum = 0, i=0, now_sum=0;
	char string[STRING_LENGTH];
	
	printf("Enter a string: ");
	fgets(string,STRING_LENGTH,stdin);
	while (string[i])
	{
		if (string[i] >= '0'&&string[i] <= '9')
		{
			now_sum = now_sum * 10 + (string[i]-'0');
		}
		else
		{
			sum += now_sum;
			now_sum = 0;
		}
		i++;
	}
	printf("Sum of numbers in string = %d\n", sum);
	
	return 0;
}