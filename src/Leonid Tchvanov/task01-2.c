#include <stdio.h>

int main()
{
    int foot,inch;
    float centimeters = 0;
    printf("Enter a height:");
    scanf("%d'%d", &foot, &inch);
    if ((foot<0) || (inch<0) || (inch>=12))
    {
        printf("Error: incorrect input value!");
        return 1;
    }
    else
    {
        centimeters = foot*30.48 + inch*2.54;
        printf("Your height in centimeters - %.2f", centimeters);
        return 0;
    }
}
